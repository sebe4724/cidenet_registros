package software;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

public class consulta_empleados extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					consulta_empleados frame = new consulta_empleados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;
	private JTextField correo;
	private JTable listaempleados;
	private JTextField buscarid;
	private JTextField buscarap1;
	private JTextField buscarap2;
	private JTextField buscarnom1;
	private JTextField buscarno2;
	private JTextField tipodoc;
	private JTextField numdoc;

	public consulta_empleados() {
		conn = (Connection) DB_cidenet.dbconnect();
		setTitle("Cidenet s.a.s");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 755, 506);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInicio = new JLabel("INICIO");
		lblInicio.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblInicio.setBounds(344, 11, 73, 30);
		contentPane.add(lblInicio);

		JLabel lblNewLabel_1_2 = new JLabel("Filtrar por");
		lblNewLabel_1_2.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1_2.setBounds(36, 64, 144, 18);
		contentPane.add(lblNewLabel_1_2);

		correo = new JTextField();
		correo.setBounds(36, 322, 96, 20);
		contentPane.add(correo);
		correo.setColumns(10);

		buscarid = new JTextField();
		buscarid.setBounds(36, 93, 96, 20);
		contentPane.add(buscarid);
		buscarid.setColumns(10);

		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int a;
				PreparedStatement stmt1;
				try {

					String query = "SELECT * FROM empleados",
							idq="",
							buscarap1q ="",
							buscarap2q = "", 
							buscarnom1q ="",
							buscarno2q ="",
							tipodocq="",  
							numdocq = "", 
							correoq ="";
					        boolean  where =  false;
					
					// Hay datos en el campo id?
					if (!buscarid.getText().equals("")) {
						where = true;
						query = query + " WHERE id = " + buscarid.getText();

					}
					
					// si no es vacio entonces query + id

					if (!buscarap1.getText().equals("")) {
						if(!where) {
							where = true;
							query = query + " WHERE primer_apellido = " + buscarap1.getText();
						}else {
							query = query + " OR primer_apellido = " + buscarap1.getText();

						}
					}
					
					if (!buscarap2.getText().equals("")) {
						if(!where) {
							where = true;
							query = query +    " WHERE segundo_apellido = " + buscarap2.getText();
						}else {
							query = query + " OR segundo_apellido = " + buscarap2.getText();

						}
						
					}				
					if (!buscarnom1.getText().equals("")) {
						if(!where) {
							where = true;
							query = query + " WHERE primer_nombre = " + buscarnom1.getText();
						}else {
							query = query + " OR primer_nombre = " + buscarnom1.getText();
						}
						
					}				
					if (!buscarno2.getText().equals("")) {
						if(!where) {
							where = true;
							query = query +    " WHERE otros_nombres = " + buscarno2.getText();
						}else {
							query = query + " OR otros_nombres = " + buscarno2.getText();

						}
						
					}
					if (!tipodoc.getText().equals("")) {
						if(!where) {
							where = true;
							query = query +    " WHERE tipo_identificación = " + tipodoc.getText();
						}else {
							query = query +  " OR tipo_identificación = " + tipodoc.getText();

						}
						
					}
					if (!numdoc.getText().equals("")){
						if(!where) {
							where = true;
							query = query +    " WHERE numero_identificacion = " + numdoc.getText();
						}else {
							query = query +    " OR tipo_identificación = " + tipodoc.getText();

						}
						
					}					
					if (!correo.getText().equals("")){
						if(!where) {
							where = true;
							query = query +  " WHERE correo_electronico = " + correo.getText();
						}else {
							query = query + " OR correo_electronico = " + correo.getText();

						}
						
					}
					
					stmt1 = (PreparedStatement) conn.prepareStatement(query);
					ResultSet rs = stmt1.executeQuery();
					ResultSetMetaData rd = (ResultSetMetaData) rs.getMetaData();
					a = rd.getColumnCount();
					DefaultTableModel df = (DefaultTableModel) listaempleados.getModel();
					df.setRowCount(0);
					while (rs.next()) {
						Vector v2 = new Vector();
						for (int i = 1; i <= a; i++) {
							v2.add(rs.getString("id"));
							v2.add(rs.getString("primer_apellido"));
							v2.add(rs.getString("segundo_apellido"));
							v2.add(rs.getString("primer_nombre"));
							v2.add(rs.getString("otros_nombres"));
							v2.add(rs.getString("tipo_identificación"));
							v2.add(rs.getString("numero_identificacion"));
							v2.add(rs.getString("correo_electronico"));

						}
						df.addRow(v2);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		btnNewButton.setBounds(44, 363, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Agregar nuevo");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregar_empleado consulta = new agregar_empleado();
				consulta.setVisible(true);
			}
		});
		btnNewButton_1.setBounds(18, 397, 135, 23);
		contentPane.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(190, 57, 539, 388);
		contentPane.add(scrollPane);

		listaempleados = new JTable();
		listaempleados.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		scrollPane.setViewportView(listaempleados);
		listaempleados.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "id", "primer_apellido", "segundo_apellido", "primer_nombre", "otros_nombres",
						"tipo_identificación", "numero_identificacion", "correo_electronico" }));

		buscarap1 = new JTextField();
		buscarap1.setColumns(10);
		buscarap1.setBounds(36, 124, 96, 20);
		contentPane.add(buscarap1);

		buscarap2 = new JTextField();
		buscarap2.setColumns(10);
		buscarap2.setBounds(36, 159, 96, 20);
		contentPane.add(buscarap2);

		buscarnom1 = new JTextField();
		buscarnom1.setColumns(10);
		buscarnom1.setBounds(36, 195, 96, 20);
		contentPane.add(buscarnom1);

		buscarno2 = new JTextField();
		buscarno2.setColumns(10);
		buscarno2.setBounds(36, 229, 96, 20);
		contentPane.add(buscarno2);

		tipodoc = new JTextField();
		tipodoc.setColumns(10);
		tipodoc.setBounds(36, 260, 96, 20);
		contentPane.add(tipodoc);

		numdoc = new JTextField();
		numdoc.setColumns(10);
		numdoc.setBounds(36, 291, 96, 20);
		contentPane.add(numdoc);
	}
}