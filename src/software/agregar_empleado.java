package software;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class agregar_empleado extends JFrame {

	private JPanel contentPane;
	private JTextField textField_ape1;
	private JTextField textField_ape2;
	private JTextField textField_nom1;
	private JTextField textField_nom2;
	private JTextField textField_documento;
	private JTextField textField_mail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					agregar_empleado frame = new agregar_empleado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creación del frame
	 */
	Connection conn = null;

	public agregar_empleado() {
		conn = (Connection) DB_cidenet.dbconnect();
		setTitle("Cidenet s.a.s");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 444, 497);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("REGISTRO");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewLabel.setBounds(142, 22, 112, 30);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Primer nombre");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(70, 200, 93, 18);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_1_1 = new JLabel("Otros nombres");
		lblNewLabel_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1_1.setBounds(70, 250, 134, 18);
		contentPane.add(lblNewLabel_1_1);

		JLabel lblNewLabel_1_2 = new JLabel("Primer apellido");
		lblNewLabel_1_2.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1_2.setBounds(71, 100, 144, 18);
		contentPane.add(lblNewLabel_1_2);

		JLabel lblNewLabel_1_3 = new JLabel("Segundo apellido");
		lblNewLabel_1_3.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1_3.setBounds(70, 150, 134, 18);
		contentPane.add(lblNewLabel_1_3);

		JComboBox lista_docs = new JComboBox();
		
		lista_docs.setModel(new DefaultComboBoxModel(new String[] { "C\u00E9dula de Ciudadan\u00EDa",
				"C\u00E9dula de Extranjer\u00EDa", "Pasaporte", "Permiso Especial" }));
		lista_docs.setBounds(70, 306, 134, 22);
		contentPane.add(lista_docs);

		textField_ape1 = new JTextField();
		textField_ape1.setBounds(225, 100, 118, 20);
		contentPane.add(textField_ape1);
		textField_ape1.setColumns(10);

		textField_ape2 = new JTextField();
		textField_ape2.setColumns(10);
		textField_ape2.setBounds(225, 150, 118, 20);
		contentPane.add(textField_ape2);

		textField_nom1 = new JTextField();
		textField_nom1.setColumns(10);
		textField_nom1.setBounds(225, 200, 118, 20);
		contentPane.add(textField_nom1);

		textField_nom2 = new JTextField();
		textField_nom2.setColumns(10);
		textField_nom2.setBounds(225, 250, 118, 20);
		contentPane.add(textField_nom2);

		textField_documento = new JTextField();
		textField_documento.setColumns(10);
		textField_documento.setBounds(225, 307, 118, 20);
		contentPane.add(textField_documento);

		textField_mail = new JTextField();
		textField_mail.setColumns(10);
		textField_mail.setBounds(225, 361, 118, 20);
		contentPane.add(textField_mail);

		JLabel lblNewLabel_1_1_1 = new JLabel("Email");
		lblNewLabel_1_1_1.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel_1_1_1.setBounds(70, 364, 134, 18);
		contentPane.add(lblNewLabel_1_1_1);
		

		
		/*
		 * Configuración del botón "agregar" el cual guarda la informacion de cada nuevo
		 * empleado en la base de datos
		 */
		Button btn_agregar = new Button("Agregar");
		btn_agregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					String apel1 = textField_ape1.getText();
					String apel2 = textField_ape2.getText();
					String nomb1 = textField_nom1.getText();
					String nomb2 = textField_nom2.getText();
					String list = (String) lista_docs.getSelectedItem();
					System.out.println(list);
					String doc = textField_documento.getText();
					String mail = textField_mail.getText();
					String query = "INSERT INTO `empleados` " 
							+ " (`primer_apellido`, `segundo_apellido`, `primer_nombre`, `otros_nombres`,  `tipo_identificación`, `numero_identificacion`, `correo_electronico`)" + 
							" VALUES ('"+ apel1 + "','" + apel2 + "','" + nomb1 + "','" +  nomb2 + "','" + list+ "','" + doc + "','" + mail+ "')";
					System.out.println(query);
					PreparedStatement pst = (PreparedStatement) conn.prepareStatement(query);
					/*pst.setString(1, apel1);
					pst.setString(2, apel2);
					pst.setString(3, nomb1);
					pst.setString(4, nomb2);
					pst.setString(5, list);
					pst.setString(6, doc);
					pst.setString(7, mail);*/
					pst.executeUpdate(); 
					JOptionPane.showMessageDialog(null, "Has agregado un nuevo usuario correctamente");
					textField_ape1.setText("");
					textField_ape2.setText("");
					textField_nom1.setText("");
					textField_nom2.setText("");
					textField_documento.setText("");
					textField_mail.setText("");
				
				} catch (Exception e) {
					System.out.println(e);
					
				}
			}
		});
		btn_agregar.setFont(new Font("Arial", Font.BOLD, 9));
		btn_agregar.setBounds(168, 413, 70, 22);
		contentPane.add(btn_agregar);
		
		

	}
}
